<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('hello', function () {
    return response()->json("hello world", 200);
});
Route::post('uploadfile', function (Request $request) {
    $file = $request->file('file');
    $name=$file->getClientOriginalName();
    $path=$request->path;

    $filePath;
    if(substr($path,-1)== '\\')
    {
        $filePath = $request->path. $name;
    }
    else{
        $filePath = $request->path.'\\'. $name;
    }
    
    Config::set('filesystems.disks.s3.bucket', $request->bucket); 
    $s3 = Storage::disk('s3');

    $s3->put($filePath, file_get_contents($file));
    //return back()->with('success','Image Uploaded successfully');
    return response()->json([
        'file'=> $name,
        'bucket'=> $request->bucket,
        'path'=>$filePath,
    ], 200);
});
